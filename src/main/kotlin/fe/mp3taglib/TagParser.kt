package fe.mp3taglib

import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.tag.FieldKey
import java.io.File
import java.util.*

object TagParser {
    const val UNKNOWN_ALBUM = "Unknown Album"
    const val UNKNOWN_ARTIST = "Unknown Artist"
    const val UNKNOWN_YEAR = "Unknown Year"
    const val UNKNOWN_GENRE = "Unknown Genre"
    const val UNKNOWN_TRACK = "Unknown Track"

    fun getTags(file: File, originalFileName: String): TagInfo? {
        val audioFile = AudioFileIO.read(file)
        val tag = audioFile.tag
        val header = audioFile.audioHeader

        if (tag != null) {
            val title = with(tag.getFirst(FieldKey.TITLE)) {
                this ?: originalFileName
            }

            val albumName = with(tag.getFirst(FieldKey.ALBUM)) {
                if (this == null || this.isEmpty()) UNKNOWN_ALBUM else this
            }

            val track = with(tag.getFirst(FieldKey.TRACK)) {
                if (this != null && this.isNotEmpty()) {
                    var track = removeLeadingZero(this)
                    if (track.contains("/")) {
                        track = track.substring(0, track.indexOf("/"))
                    }

                    return@with track
                } else UNKNOWN_TRACK
            }

            val year = with(tag.getFirst(FieldKey.YEAR)) {
                if (this == null || this.isEmpty()) {
                    UNKNOWN_YEAR
                } else this
            }

            val genre = with(tag.getFirst(FieldKey.GENRE)) {
                if (this == null || this.isEmpty()) UNKNOWN_GENRE
                else this
            }

            val artist = with(tag.getFirst(FieldKey.ARTIST)) {
                if (this == null || this.isEmpty()) UNKNOWN_ARTIST else this
            }

            val artwork = with(tag.firstArtwork) {
                if (this != null) TagAlbumArt(this.binaryData, this.mimeType) else null
            }

            return TagInfo(
                header.preciseTrackLength,
                title,
                albumName,
                ArtistTagHelper.findAllArtists(title, artist),
                artist,
                genre,
                track,
                year,
                artwork
            )
        }

        return null
    }

    @JvmStatic
    fun getNullableTags(audioFile: File, originalFileName: String) =
        Optional.ofNullable(getTags(audioFile, originalFileName))

    private fun removeLeadingZero(str: String): String {
        var newStr = str
        while (newStr.startsWith("0")) {
            newStr = newStr.substring(1)
        }

        return newStr
    }
}

data class TagInfo(
    val length: Double,
    val title: String,
    val album: String,
    val artist: MutableMap<String, MutableSet<String>>,
    val originalArtistTagString: String,
    val genre: String,
    val track: String,
    val year: String,
    val albumImage: TagAlbumArt?
)

data class TagAlbumArt(val bytes: ByteArray, val mimeType: String) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TagAlbumArt

        if (!bytes.contentEquals(other.bytes)) return false
        if (mimeType != other.mimeType) return false

        return true
    }

    override fun hashCode(): Int {
        var result = bytes.contentHashCode()
        result = 31 * result + mimeType.hashCode()
        return result
    }
}
