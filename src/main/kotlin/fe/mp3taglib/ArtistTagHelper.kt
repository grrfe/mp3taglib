package fe.mp3taglib

object ArtistTagHelper {
    const val NO_ARTIST_GROUP = "NO_ARTIST_GROUP"

    private val TITLE_FEATURE_INDICATORS = listOf("ft.", "feat.", "featuring.")
    private val ARTIST_DELIMITERS = listOf(",", ";")
    private val GROUP_INDICATORS = listOf(" :of: ", " :from: ")

    private val PARENTHESIS = mapOf('(' to ')', '[' to ']')

    fun findAllArtists(titleTag: String?, artistTag: String?): MutableMap<String, MutableSet<String>> {
        val artists = getTitleArtists(titleTag).plus(splitFeaturedArtists(artistTag)).toMutableSet()
        val ofGroupIndices = findGroupIndices(artists)

        val artistGroupMap = mutableMapOf<String, MutableSet<String>>()

        var lastIdx = 0
        val removeGroupArtistList = mutableListOf<String>()

        ofGroupIndices.forEach { (i, group) ->
            val index = i + 1
            artists.subSet(lastIdx, index).forEach { artist ->
                artistGroupMap.getOrPut(group) { mutableSetOf() }
                    .add(splitByDelimiter(artist, GROUP_INDICATORS)[0].trim())
                removeGroupArtistList.add(artist)
            }

            lastIdx = index
        }

        artists.removeAll(removeGroupArtistList)

        return artistGroupMap.apply {
            this[NO_ARTIST_GROUP] = artists
        }
    }

    fun getTitleArtists(title: String?): List<String> {
        if (title != null) {
            for (feature in TITLE_FEATURE_INDICATORS) {
                if (title.contains(feature)) {
                    val featStartIdx = title.indexOf(feature)
                    var closingBracket: Char? = null

                    if (featStartIdx - 1 > 0) {
                        val suspectedBracket = title[featStartIdx - 1]
                        PARENTHESIS[suspectedBracket]?.let {
                            closingBracket = it
                        }
                    }

                    val featuredArtistIdx = featStartIdx + feature.length
                    if (featuredArtistIdx < title.length) {
                        val featuredArtists = with(title.substring(featuredArtistIdx)) {
                            if (closingBracket != null) {
                                this.substring(0, this.indexOf(closingBracket!!))
                            } else this
                        }

                        return splitFeaturedArtists(featuredArtists)
                    }
                }
            }
        }

        return emptyList()
    }

    private fun splitFeaturedArtists(featuredArtists: String?): List<String> {
        return if (featuredArtists != null) {
            splitByDelimiter(featuredArtists, ARTIST_DELIMITERS).map { sanitizeArtistName(it) }
        } else emptyList()
    }

    private fun splitByDelimiter(str: String, delimiters: List<String>): List<String> {
        return str.split(*delimiters.filter { str.contains(it) }.toTypedArray())
    }

    private fun sanitizeArtistName(name: String): String {
        var retName = name.trim()

        PARENTHESIS.forEach { entry ->
            retName = replaceSimple(retName, entry.value.toString())
        }

        return retName
    }

    private fun replaceSimple(name: String, lastToken: String): String {
        return if (name.endsWith(lastToken)) name.replace(lastToken, "") else name
    }

    private fun findGroupIndices(artists: Set<String>): Map<Int, String> {
        return artists.mapIndexedNotNull { index, artist ->
            GROUP_INDICATORS.firstOrNull { indicator ->
                artist.contains(indicator)
            }?.let {
                return@let index to artist.afterSubstring(it)
            }
        }.toMap()
    }
}

fun <T> MutableSet<T>.subSet(fromIndex: Int, toIndex: Int): Set<T> {
    return this.asSequence().drop(fromIndex).take(toIndex - fromIndex).toSet()
}

fun String.afterSubstring(str: String): String {
    return this.substring(this.indexOf(str) + str.length)
}

//fun <T> MutableList<T>.addIfNotContains(item: T) {
//    if (item !in this) {
//        this.add(item)
//    }
//}