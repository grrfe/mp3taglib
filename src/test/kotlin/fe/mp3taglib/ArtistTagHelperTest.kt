package fe.mp3taglib

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable

internal class ArtistTagHelperTest {
    data class Tag(val titleTag: String, val artistTag: String)
    class Match(val group: String, vararg val artists: String)

    private val testData = mapOf(
        Tag("Bikini Bottom Gangster (feat. Patrick Bang)[Explicit]", "Spongebozz") to listOf(
            Match(ArtistTagHelper.NO_ARTIST_GROUP, "Patrick Bang", "Spongebozz")
        ),
        Tag("Bikini Bottom Gangster feat. Patrick Bang", "Spongebozz") to listOf(
            Match(ArtistTagHelper.NO_ARTIST_GROUP, "Patrick Bang", "Spongebozz")
        ),
        Tag("The Middle (feat. Maren Morris, Grey)", "Zedd") to listOf(
            Match(ArtistTagHelper.NO_ARTIST_GROUP, "Maren Morris", "Grey", "Zedd")
        ),
        Tag("Welcome to Chilis (feat. bbno$)", "Yung Gravy") to listOf(
            Match(ArtistTagHelper.NO_ARTIST_GROUP, "bbno\$", "Yung Gravy")
        ),
        Tag("Empire feat. Minnie :of: (G)I-DLE", "WENGIE") to listOf(
            Match(ArtistTagHelper.NO_ARTIST_GROUP, "WENGIE"),
            Match("(G)I-DLE", "Minnie")
        ),
        Tag("Really Really", "Siyeon, Sua, Yoohyeon :of: Dreamcatcher") to listOf(
            Match(ArtistTagHelper.NO_ARTIST_GROUP),
            Match("Dreamcatcher", "Siyeon", "Sua", "Yoohyeon")
        ),
        Tag("Warzone (feat. Monty Xon)", "Bri-C; Monty Xon") to listOf(
            Match(ArtistTagHelper.NO_ARTIST_GROUP, "Monty Xon", "Bri-C")
        ),
        Tag("Open Season", "NOK of the Future") to listOf(
            Match(ArtistTagHelper.NO_ARTIST_GROUP, "NOK of the Future")
        ),
        Tag("i still think of u ft. Kam Michael", "Ouse") to listOf(
            Match(ArtistTagHelper.NO_ARTIST_GROUP, "Kam Michael", "Ouse")
        ),
        Tag("Remembrance", "Myth & Roid") to listOf(
            Match(ArtistTagHelper.NO_ARTIST_GROUP, "Myth & Roid")
        )
    )

    @Test
    fun testFindAllArtists() {
        testData.forEach { (tag, matches) ->
            val artists = ArtistTagHelper.findAllArtists(tag.titleTag, tag.artistTag)

            Assertions.assertAll(matches.map { match ->
                Executable {
                    Assertions.assertEquals(artists[match.group], match.artists.toSet())
                }
            })
        }
    }
}

