# mp3taglib

The main purpose of this library is to provide a simple interface to parse tags from various music formats. 
Under the hood, [jaudiotagger](https://github.com/Kaned1as/jaudiotagger) is used.

The library also provides an artist tag parser which can parse featured artists from title/artist tags as well as artists from bands/groups.

